import geotools.Vector as Vector

class Position:
    def __init__(self,x,y,z):
        self._x = x
        self._y = y
        self._z = z
        
    def getX(self):
        return self._x
    
    def getY(self):
        return self._y
    
    def getZ(self):
        return self._z
    
    def distance(self,pos):
        from math import sqrt
        dx = self._x - pos.getX()
        dy = self._y - pos.getY()
        dz = self._z - pos.getZ()
        return sqrt(dx**2 + dy**2 + dz**2)
    
    def vector_to(self,pos):
        return Vector(pos.getX() - self._x, pos.getY() - self._y, pos.getZ() - self._z)
