from math import sqrt

class Vector:
    def __init__(self,x,y,z):
        self._x = x
        self._y = y
        self._z = z
        
    def get_length(self):
        return sqrt(self._x**2 + self._y**2 + self._z**2)
    
    def scale(self,scale):
        self._x *= scale
        self._y *= scale
        self._z *= scale

    def scalar_product(self,vector):
        x = self._x * vector._x
        y = self._y * vector._y
        z = self._z * vector._z

        return sqrt(x+y+z)
        
    def to_unity(self):
        import copy
        result = copy.deepcopy(self)
        result.scale(1 / result.get_length())
        return result

    def as_list(self):
        return [self._x,self._y,self._z]
    
